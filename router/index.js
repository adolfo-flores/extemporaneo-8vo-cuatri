import express from "express";
import json from 'body-parser';
export const router = express. Router();

router.get('/',(req,res)=>{
    res.render('index',{titulo:"Mi primera pagina ejs",nombre:"Adolfo Flores"});
});

router.get('/contacto',(req,res)=>{

    const params = {

    }
    res.render('contacto',params);
})

router.get('/pago',(req,res)=>{

    const params = {

        numR : req.query.numR || '',
        nombre : req.query.nombre || '',
        domicilio : req.query.domicilio || '',
        killowattsC : req.query.killowattsC || '',
        servicioT : req.query.servicioT || ''

    }
    res.render('pago',params);
})

router.post('/pago',(req,res)=>{

    const params = {

        numR : req.query.numR || '',
        nombre : req.query.nombre || '',
        domicilio : req.query.domicilio || '',
        killowattsC : req.query.killowattsC || '',
        servicioT : req.query.servicioT || ''

    }
    res.render('pago',params);
})

router.get('/index',(req,res)=>{

    const params = {

    }
    res.render('index',params);
})

router.get('/recibo',(req,res)=>{

    const params = {

        nombre: '',
        servicioT: '',
        killowattsC: '',
        costo: ''

    }
    res.render('recibo',params);
})

router.post('/recibo',(req,res)=>{

    let costo = 0;
    let servicioT = req.body.servicioT;
    if(servicioT === "1"){

        costo = 1.08;
        servicioT = "Domestico";

    }else if(servicioT === "2"){

        costo = 2.5;
        servicioT = "Comercial";

    }else if(servicioT === "3"){

        costo = 3.0;
        servicioT = "Industrial";

    }

    const params = {

        nombre: req.body.nombre || '',
        servicioT: servicioT || '',
        killowattsC: req.body.killowattsC || '',
        costo: costo

    }

    console.log(params);
    res.render('recibo',params);
})

export default { router }//